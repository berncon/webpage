sap.ui.define([ "sap/ui/core/mvc/Controller", "sap/m/MessageToast"], function(Controller, MessageToast,
		JSONModel,ResourceModel) {
	"use strict";
	return Controller.extend("ch.berncon.controller.App", {
		
		onInit : function() {
			let oBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();
			let sMsg = oBundle.getText("welcomeToast");
			MessageToast.show(sMsg);
		}
	
	});
});

