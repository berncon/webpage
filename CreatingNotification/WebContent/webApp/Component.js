sap.ui.define([
   "sap/ui/core/UIComponent",
   "sap/ui/model/json/JSONModel"
], function (UIComponent, JSONModel, ResourceModel) {
   "use strict";
   return UIComponent.extend("ch.berncon.Component",{
  	 metadata : {
       manifest: "json",
      	 propagateModel: true
  	 },
      init : function () {
         // call the init function of the parent
         UIComponent.prototype.init.apply(this, arguments);
         
         // set data model
         var oData = {
            recipient : {
               name : "World"
            }
         };
         var oModel = new JSONModel(oData);
         this.setModel(oModel);

         
      }
	});
});